import qliphortClient
import pygame
import player
import le_map
import bombs

pygame.init()
 
screen = pygame.display.set_mode((640, 480))
clock = pygame.time.Clock()

le_map = le_map.Map(screen)
bomb = bombs.Bomb(screen)

#controlled player
unit = player.Player(screen,le_map,bomb,"assets/player2.png")
unit.position.x = 320
unit.position.y = 320

#enemy
unit2 = player.Player(screen,le_map,bomb,"assets/player.png")
unit2.position.x = 32
unit2.position.y = 32

game_over = False

qliphortClient.start(unit, unit2, le_map, bomb)

while game_over == False:
 
    for event in pygame.event.get():
        #print (event)
        if event.type == pygame.QUIT:
            game_over = True
        unit.keyboardInput(event)

    
    screen.fill(pygame.Color('green'))  
    le_map.animate()
    unit.animate()
    unit2.animate()
    if le_map.isGameOver(unit,unit2) == True:
        game_over = True
    pygame.display.flip()              
    clock.tick(10)
 
pygame.quit ()

