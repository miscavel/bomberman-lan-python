#client program
import socket


HOST = '127.0.0.1'    # The remote host
PORT = 16018          # The same port as used by the server
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
while True:
    message = input("Enter your message = ")
    if message == "exit" :
        break;
    s.send(message.encode('utf-8'))
    data = s.recv(1024)
    print ('Received', data.decode('utf-8'))
s.close()

