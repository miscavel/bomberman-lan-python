import _thread
import time
import socket

connected = True

def qliphort_stabilizer( qliphort, delay):
        while True:
                time.sleep(delay)
                try:
                        qliBuffer = " "
                        qliphort.send(qliBuffer.encode('UTF-8'))
                except:
                        print ('Unstable')
                        break
        qliphort.close()


def receive_msg( qliphort, addr, delay):
        while True:
                #print "Recv"
                #time.sleep(delay)
                try:
                        data = qliphort.recv(1024)
                        data = data.decode()
                        data = data.strip()
                        if data != "":
                                print (addr, ': ', data)
                except:
                        break


def send_msg( qliphort, delay, connected):
        while True:
                #print "Send"
                time.sleep(delay)
                #input_send = input()
                input_send = 'Player 1 : 30 30'
                #if input_send == '-q':
                #       connected=False
                #       break
                try:
                        qliphort.send(input_send.encode('UTF-8'))
                except:
                        break
        qliphort.close()


s = socket.socket()
host = socket.gethostname()
port = 32135
s.bind((host, port))

connection_id=0
s.listen(5)
while connected:
        try:
                c, addr = s.accept()
                addr_string = addr[0]
                print ("Masuk", addr[0])
                _thread.start_new_thread( qliphort_stabilizer, (c, 1, ) )
                _thread.start_new_thread( receive_msg, (c, addr_string, 1, ) )
                _thread.start_new_thread( send_msg, (c, 1, connected, ) )
        except:
                pass
