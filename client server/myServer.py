#server program
import socket
from threading import *

HOST = ''                 # Symbolic name meaning all available interfaces
PORT = 16018              # Arbitrary non-privileged port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(2)

class client(Thread):
    def __init__(self,socket,address):
        Thread.__init__(self)
        self.socket = socket
        self.address = address
        self.start()

    def run(self):
            while True:
                data = self.socket.recv(1024).decode('utf-8')
                if not data:
                    self.socket.close()
                    break
                print ("Client - ",self.address," sent : ",data)
                reply = "Thanks for what you send to me"
                self.socket.sendall(reply.encode('utf-8'))
        
count = 0
while True:
    socket, address = s.accept()
    client(socket,address)
