import _thread
import time
import socket
import sys

mapCoord = []
playerX = "San Holo"
playerY = "Han Solo"
playerX2 = "Iris"
playerY2 = "Grimoire"
connected = True
updating = False
mapPlunge = ""
bombX = "Tamer"
bombY = "Plateau"
bombX2 = "Lucina"
bombY2 = "Falchion"
player1 = "binkwdll"
player2 = "binkwdll"
player1con = False
player2con = False
p1update = True
p2update = True
connection_id = 0
player1client = ""
player2client = ""

def receive_msg( qliphort, addr, delay, connection_idv):
        global player1
        global player2
        global playerX
        global playerY
        global playerX2
        global playerY2
        global bombX
        global bombY
        global bombX2
        global bombY2
        global mapCoord
        global updating
        global mapPlunge
        global p1update
        global p2update
        global connection_id
        global player1con
        global player2con
        
        while True:
                send = False
                sep = 'o'
                try:
                        data = qliphort.recv(37)
                        if connection_idv == 0:
                                while p1update == False:
                                        if player2con == False:
                                                break
                                        pass
                                player1 = data
                                p1update = False
                        else:
                                while p2update == False:
                                        if player1con == False:
                                                break
                                        pass
                                player2 = data
                                p2update = False

                        data = qliphort.recv(332)
                        if connection_idv == 0:
                                mapPlunge = data
                        send_msg( qliphort, delay, connection_idv)
                except:
                        print ('Player',connection_idv+1,' has quit the game')
                        if connection_idv == 0:
                                player1con = False
                        else:
                                player2con = False
                        qliphort.close()
                        connection_id -= 1
                        break
                        
        
def send_msg( qliphort, delay, connection_idv):
        global player1
        global player2
        global playerX
        global playerY
        global playerX2
        global playerY2
        global mapCoord
        global updating
        global mapPlunge
        global p1update
        global p2update
        global player1con
        global player2con

        try:
                while player1 == "binkwdll" or player2 == "binkwdll":
                        pass
        
                if connection_idv == 0:
                        qliphort.send(player2)
                        p2update = True
                else:
                        qliphort.send(player1)
                        p1update = True
                qliphort.send(mapPlunge)
        except:
                pass

def start():
        global playerX
        global connection_id
        global player1con
        global player2con
        global player1client
        global player2client
        
        s = socket.socket()
        host = socket.gethostname()
        #host = "0.0.0.0"
        port = 32135
        s.bind((host, port))

        #print(host)

        connection_id=0
        s.listen(5)
        s.settimeout(1.0)
        #print(s.gettimeout())
        while connected:
                try:
                        c, addr = s.accept()
                        addr_string = addr[0]
                        if connection_id < 2:
                                print ("Player",(connection_id+1)," has entered the game :",addr[0])
                                if connection_id == 0:
                                        player1client = c
                                        c.send(('1p32op32o').encode('UTF-8'))
                                        player1con = True
                                else:
                                        player2client = c
                                        c.send(('2p352p384').encode('UTF-8'))
                                        player2con = True
                                _thread.start_new_thread( receive_msg, (c, addr_string, 1, connection_id) )
                                connection_id += 1
                                
                                if player1con == True and player2con == True:
                                        print("All players gathered! Beginning the match..")
                                        player1client.send(('go').encode('UTF-8'))
                                        player2client.send(('go').encode('UTF-8'))
                        else:
                                c.send(('123456789').encode('UTF-8'))
                                c.close()
                except:
                        pass


start()
