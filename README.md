# Bomberman LAN Python

An attempt to create a multiplayer (LAN) bomberman game.

Current host IP config is set to localhost for showcase purposes, if specific IP is desired the host IP can be set in qliphortServer.py and qliphortClient.py under 'host' variable.

![Preview Video](video/preview.mp4)


Credits :

Bomberman Game - Nemeria & Ashurali

Socketing - Miscavel